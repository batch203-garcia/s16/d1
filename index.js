/* console.log("Hello, World!") */

// [SECTION] Arithmetic Operators (+, -, * , /)

let x = 100;
let y = 25;

// Addition
let sum = x + y;
console.log("Result of addition operator: " + sum);

//subtraction
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

//multiplication
let product = x * y;
console.log("Result of multiplication operator: " + product);

//division
let quotient = x / y;
console.log("Result of division operator: " + quotient);

//Modulo -> getting remainder -> let x = 101; result should be 1
let modulo = x % y;
console.log("Result of modulo operator: " + modulo);

//[SECTION] Assignment Operators 
//Basic Assignemnt Operator (=)

let assignmentNumber = 8;

console.log("The current value of assignementNumber variable: " + assignmentNumber);

//Addition Assignemnt Operator
assignmentNumber = assignmentNumber + 2; //long method
//assignmentNumber += 2; short method
console.log("Result of addition assignementNumber operator: " + assignmentNumber);

//Sub/Mult/Div (-=,*=,/=)

assignmentNumber -= 2;
console.log("Result of subtraction assignementNumber operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplicaiton assignementNumber operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignementNumber operator: " + assignmentNumber);

//[SECTION] PEMDAS (Order of Operation) 

/* 
    The operation were done in the following order:
    1. 3*4 = 12 -> 1+2-12/5
    2. 12/5 = 2.4 -> 1+2-2.4
    3. 1+2 = 3 -> 3-24
    4. 3-2.4 = 0.6000000000000001 --> result
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas)

/* 
    The operation were done in the following order:
    1. (4 / 5) = 0.8 -> 1 + (2 - 3) * 0.8
    2. (2 - 3) = -1 -> 1 + (-1) * 0.8
    3. (-1) * 0.8 =  -0.8
    4. 1 - 0.8 = 0.20 -> result

*/
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas) //.20

//[SECTION] Increment and dcrement
//Operators that add or subtact by 1 (++) and (--)

//Increment
let z = 1;
let increment = ++z;
//1+1
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment for z: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment for z: " + z);

//Decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement)
console.log("Result of pre-decrement for z: " + z);

increment = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement for z: " + z);

//[SECTION] Type Coercion
//convertion of data type

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion);

//boolean
let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

//[SECTION] Comparison Operator

let juan = "juan";

//Equality Operator (==)

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
//compare string with same value -> case sensitive
console.log('juan' == 'juan'); //true
//compare string to a variable value -> let juan = "juan"
console.log('juan' == juan); //true

//Inequality Operator (!=)
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false

//Strict Equality Operator (===) -> equal value and same type
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(0 === false); //false
console.log('juan' === 'juan'); //true
console.log('juan' === juan); //true

//Strict Inequality Operator (!==)
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan'); //false
console.log('juan' !== juan); //false

//[SECTION] Relational Operator (< or >)

let a = 50;
let b = 65;

//Greater Than Operation (>)

let isGreaterThan = a > b; //50 > 65 = false
console.log(isGreaterThan);

//Less Than Operation (<)
let isLessThan = a < b; //50 < 65 = true
console.log(isLessThan);

//Greater Than or Equal Operation (>=)
let isGTOrEqual = a >= b; //50 >= 65 = false
console.log(isGTOrEqual);

//Less Than or Equal Operation (>=)
let isLTOrEqual = a <= b; //50 <= 65 = true
console.log(isLTOrEqual);

let numString = "30";
console.log(a > numString); //true -> forced coercion
console.log(a < numString); //false -> forced coercion

let str = "twenty";
console.log(a >= str); //false -> forced coercion
console.log(a <= str); //false -> forced coercion

//[SECTION] Logical Operator (AND &&)

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered; //returns true if values are true
console.log("Result of logical AND Operator: " + allRequirementsMet);

//Logical OR Operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered; //returns true if one of the values are true
console.log("Result of logical OR Operator: " + someRequirementsMet);

//Logical NOT Operator (! - Exclamation)
let someRequirementsNotMet = !isLegalAge;
console.log("Result of logical NOT Operator: " + someRequirementsNotMet);